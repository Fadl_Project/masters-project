import shutil
import os

path_name = "/home/student2//PycharmProjects/masterproject/relook/instance_occlsegm/instance_occlsegm_lib/contrib/synthetic2d/datasets/green_ai/to_be_grouped"
common_list = sorted(os.listdir(path_name))

for count, filename in enumerate(os.listdir(path_name)):
    """ rename file pair """
    if filename.endswith('npz'):    # string.endswith(value, start, end)
        new = "pic-" + str(count) + '.npz'  # new file name
        src = os.path.join(path_name, filename)  # file source
        dst = os.path.join(path_name, new)  # file destination
        # rename all the file
        os.rename(src, dst)

    elif filename.endswith('jpg'):
        new = "pic-" + str(count) + '.jpg'  # new file name
        src = os.path.join(path_name, filename)  # file source
        dst = os.path.join(path_name, new)  # file destination
        # rename all the file
        os.rename(src, dst)
    else:
        pass

    """ make directory using count index """
    dest_dir = os.path.join(path_name, "pic-" + str(count) + '.jpg')
    # create new directory if does not exist
    if not os.path.isdir(dest_dir):




    """ move file pair to directory """
    # file source and destination
    src = r'C:\py_scripts\photos\red_sky.jpg'
    dst = r'C:\py_scripts\photos\awesome\awesome_sky.jpg'

    # change file directory and rename file as awesome_sky
    shutil.move(src, dst)