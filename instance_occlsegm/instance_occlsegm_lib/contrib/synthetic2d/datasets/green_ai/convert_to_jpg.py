import os
import cv2
from pathlib import Path


# Input of images to be converted from .png format to .jpg format.
rgb_PATH = "/home/student2/Downloads/Latest_dataset/Part1/original/rgb"
# Output path for the generated .jpg images.
rgb_OUTPATH = "/home/student2/Downloads/Latest_dataset/Part1/rgb_jpg"
# Order images in 'PATH' alphabetically.
rgb_DIR_PATH = sorted(os.listdir(rgb_PATH))
# List files and directories
# in "/home/student2/Downloads/Latest_dataset/Part1/rgb_jpg"
print("Before saving image:")
print(os.listdir(rgb_OUTPATH))
for i, image_name in enumerate(rgb_DIR_PATH):
    # Index every image in 'DIR_PATH'.
    input_path = os.path.join(rgb_PATH, image_name)
    image = cv2.imread(input_path)
    image = cv2.resize(image, (640, 480), interpolation=cv2.INTER_NEAREST)
    # change directory
    os.chdir(rgb_OUTPATH)
    # Saving the image
    image_name = Path(input_path).stem
    cv2.imwrite(image_name + '.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), 95])
    if i >= 9:
        break
# List files
print("After saving images:")
print(os.listdir(rgb_OUTPATH))
print('Successfully saved')



# Input of masks images to be converted from .png format to .jpg format.
masks_PATH = "/home/student2/Downloads/Latest_dataset/Part1/original/masks"
# Output path for the generated .jpg masks.
masks_OUTPATH = "/home/student2/Downloads/Latest_dataset/Part1/masks_jpg"
# Order images in 'PATH' alphabetically.
masks_DIR_PATH = sorted(os.listdir(masks_PATH))
# List files and directories
# in "/home/student2/Downloads/Latest_dataset/Part1/masks_jpg"
print("Before saving image:")
print(os.listdir(masks_OUTPATH))
"""
test with mask image quality. but this time do not convert to .jpg, instead work with .png, but just change the name.
I think .jpg is grainy, so those grains are mistakenly recoginzed as seperate classes in the .npy files (bbox, labels and npy_lbl_cls)."""
for i, mask_name in enumerate(masks_DIR_PATH):
    # Index every image in 'DIR_PATH'.
    input_path = os.path.join(masks_PATH, mask_name)
    mask = cv2.imread(input_path)
    mask = cv2.resize(mask, (640, 480), interpolation=cv2.INTER_NEAREST)
    # change directory
    os.chdir(masks_OUTPATH)
    # Saving the image
    mask_name = Path(input_path).stem
    #cv2.imwrite(mask_name + '.jpg', mask, [int(cv2.IMWRITE_JPEG_QUALITY)])
    cv2.imwrite(mask_name + '.jpg', mask)
    if i >= 1:
        break
# List files
print("After saving images:")
print(os.listdir(masks_OUTPATH))
print('Successfully saved')