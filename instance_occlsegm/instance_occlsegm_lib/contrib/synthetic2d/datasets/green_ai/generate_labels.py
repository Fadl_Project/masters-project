import numpy as np
import os
import cv2
import matplotlib.pyplot as plt


def get_labels(img):
    hist = cv2.calcHist([img], [0], None, [256], [0, 256])
    labels = []
    [labels.append(i) for i, l in enumerate(hist) if i != 0 and l != 0]
    print('labels are: ', labels)
    return

def main():
    # define here the output path for the generated .npy mask
    outPath = "/home/student2/Downloads/Latest_dataset/Part1/masks_npy/labels"
    # define here the path of the images to be converted from .png format to .npy format
    path = "/home/student2/Downloads/Latest_dataset/Part1/masks"
    dir_path = sorted(os.listdir(path))

    # iterate through the names of contents of the folder
    for i, img in enumerate(dir_path):
        if img.endswith("im.png"):
            # create the full input path and read the file
            input_path = os.path.join(path, img)
            img = cv2.imread(input_path)

            img = cv2.imread(image_path)
            plt.imshow(img)
            plt.title('img')
            plt.show()
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            img = img / 4
            img = img.astype(np.uint8)
            plt.imshow(img)
            plt.title('img')
            plt.show()
            img = img[:, :, 0]
            plt.imshow(img)
            plt.title('img')
            plt.show()

            histogram = cv2.calcHist([img], [0], None, [256], [0, 256])
            print(type(histogram))
            plt.plot(histogram, color='r')
            plt.show()

            """
            Masks with labels begining with 3 such as 31 or 32, are for occluded masks.
            On the contrary, labels starting with 5, such as 51 or 52 are for visible maks.
            """
            labels = []
            [labels.append(i) for i, l in enumerate(histogram) if i != 0 and l != 0]
            print(type(labels))
            print(labels)

            # create full output path, so 'example.jpg' becomes 'npy_example.jpg', save the file to disk.
            fullpath = os.path.join(outPath, 'npy_label' + img)
            np.save(fullpath, mask)

if __name__ == '__main__':
    main()
