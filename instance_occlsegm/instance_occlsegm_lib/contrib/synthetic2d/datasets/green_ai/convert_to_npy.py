"""
this scripts converts the reduced-size images in .jpg format to .npy format so
they could be fed to the network.
"""

"""
the created numpy files from this script (.jpg >> .npy) are around 7.1MB
compared to 7.7MB in case of (.png >> .npy) conversion. 
storage saved is around 10% in total.
"""
import numpy as np
import os
import cv2

def main():
    # define here the output path for the generated .npy images
    outPath = "/home/student2/Downloads/Latest_dataset/Part1/jpg_npy"
    # define here the path of the images to be converted from .jpg format to .npy format
    path = "/home/student2/Downloads/Latest_dataset/Part1/jpg"
    dir_path = sorted(os.listdir(path))

    # iterate through the names of contents of the folder
    for i, image_name in enumerate(dir_path):
        if image_name.endswith("rgb.png.jpg"):
            # create the full input path and read the file
            input_path = os.path.join(path, image_name)
            image = cv2.imread(input_path)

            # BGR -> RGB
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            print(type(image))
            print(input_path)

            # create full output path, save the file to disk.
            fullpath = os.path.join(outPath, image_name)
            np.save(fullpath, image)
    print('Successfully saved')

if __name__ == '__main__':
    main()
