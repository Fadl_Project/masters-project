import numpy as np
import matplotlib.pyplot as plt
import cv2
import os

print(os.getcwd())
# img.npy masks.npy labels.npy bboxes.npy npy_lbl_cls.npy ||| *im.jpg

dir_path = 'Package_01_synthetic_10_19'
rgb_image = os.path.join(dir_path, 'Package_01_synthetic_10_19_rgb.png')
mask_image = os.path.join(dir_path, 'Package_01_synthetic_10_19_im.png')
npy_img = os.path.join(dir_path, 'img.npy')
npy_masks = os.path.join(dir_path, 'masks.npy')
npy_bboxes = os.path.join(dir_path, 'bboxes.npy')
npy_lables = os.path.join(dir_path, 'labels.npy')
npy_lbl_cls = os.path.join(dir_path, 'lbl_cls.npy')

"""
From the greenai dataset.
"""
# load a .jpg or .png image
rgb_image = cv2.imread(rgb_image)
rgb_image = cv2.cvtColor(rgb_image, cv2.COLOR_BGR2RGB)
print('green_rgb_image.dtype: ', rgb_image.dtype)
print('green_rgb_image.shape: ', rgb_image.shape)
print('')
plt.imshow(rgb_image)
plt.title('rgb_image')
plt.show()

# load a .jpg mask image
mask_image = cv2.imread(mask_image)
mask_image = cv2.cvtColor(mask_image, cv2.COLOR_BGR2RGB)
print('mask_image.dtype: ', mask_image.dtype)
print('mask_image.shape: ', mask_image.shape)
print('')
plt.imshow(mask_image)
plt.title('mask_image')
plt.show()

# load an img.npy
"""
error solved:  ValueError("Object arrays cannot be loaded when "
ValueError: Object arrays cannot be loaded when allow_pickle=False 
solution: allow_pickle=True

error: "Image data of dtype {} cannot be converted to "
TypeError: Image data of dtype object cannot be converted to float
solution: img.npy is probably saved as the image name, not the image itself in .npy format. 
"""
#img_npy = np.load('Package_01_synthetic_10_10_im.jpg/img.npy')
#img_npy = np.load('Package_01_synthetic_10_10_im.jpg/img.npy', allow_pickle=True) # Good!
npy_img = np.load(npy_img, allow_pickle=True)
#img_npy = np.load('img.npy', allow_pickle=True)

print('npy_img.dtype: ', npy_img.dtype)
print('npy_img.shape: ', npy_img.shape)
print('')
plt.imshow(npy_img)
plt.title('npy_img')
plt.show()

# load a masks.npy
npy_masks = np.load(npy_masks)
print('npy_masks.dtype: ', npy_masks.dtype)
print('npy_masks.shape: ', npy_masks.shape)
print('')
plt.imshow(npy_masks[0])
plt.title('npy_masks')
plt.show()

# load a npy_lbl_cls.npy
npy_lbl_cls = np.load(npy_lbl_cls)
print('npy_lbl_cls.dtype: ', npy_lbl_cls.dtype)
print('npy_lbl_cls.shape: ', npy_lbl_cls.shape)
print('')
print('element value: ', npy_lbl_cls[0, 0])
print('element value: ', npy_lbl_cls[-1, -1])
print('element value: ', npy_lbl_cls[479, 639])
plt.imshow(npy_lbl_cls)
plt.title('npy_lbl_cls')
plt.show()

# load a labels.npy
npy_lables = np.load(npy_lables)
print('npy_lables.dtype: ', npy_lables.dtype)
print('npy_lables.shape: ', npy_lables.shape)
print('')

# load a bboxes.npy
npy_bboxes = np.load(npy_bboxes)
print('npy_bboxes.dtype: ', npy_bboxes.dtype)
print('npy_bboxes.shape: ', npy_bboxes.shape)
print('\n############ ########### ##############\n')





"""
From the original dataset.
"""
# load an image
og_image_jpg = cv2.imread('000000/00000000.jpg')
og_image_jpg = cv2.cvtColor(og_image_jpg, cv2.COLOR_BGR2RGB)
print('og_image_jpg.dtype: ', og_image_jpg.dtype)
print('og_image_jpg.shape: ', og_image_jpg.shape)
print('')
plt.imshow(og_image_jpg)
plt.title('og_image_jpg')
plt.show()

# load an img.npy
og_img_npy = np.load('000000/img.npy')
print('og_img_npy.dtype: ', og_img_npy.dtype)
print('og_img_npy.shape: ', og_img_npy.shape)
print('')
plt.imshow(og_img_npy)
plt.title('og_img_npy')
plt.show()

# load a mask.npy
og_masks_npy = np.load('000000/masks.npy')
print('og_masks_npy.dtype: ', og_masks_npy.dtype)
print('og_masks_npy.shape: ', og_masks_npy.shape)
print('')
plt.imshow(og_masks_npy[0])
plt.title('og_masks_npy')
plt.show()

# load a npy_lbl_cls.npy
"""
instance intensity in npy_lbl_cls.npy  = (instance class (number) - 1). e.g,
background -1
Intensity  0 = class number  1 : __background__
Intensity 27 = class number 28 : plastic_wine_glass
Intensity 24 = class number 25 : mesh_cup
"""
og_lbl_cls_npy = np.load('000000/lbl_cls.npy')
print('og_lbl_cls_npy.dtype: ', og_lbl_cls_npy.dtype)
print('og_lbl_cls_npy.shape: ', og_lbl_cls_npy.shape)
print('')
plt.imshow(og_lbl_cls_npy)
plt.title('og_lbl_cls_npy')
plt.show()

print('element value: ', og_lbl_cls_npy[0, 0])
print('element value: ', og_lbl_cls_npy[-1, -1])
print('element value: ', og_lbl_cls_npy[479, 639])

# load a labels.npy
labels = np.load('000000/labels.npy')
print('labels.dtype: ', labels.dtype)
print('labels.shape: ', labels.shape)
print('')

# load a bboxes.npy
bboxes = np.load('000000/bboxes.npy')
print('bboxes.dtype: ', bboxes.dtype)
print('bboxes.shape: ', bboxes.shape)
print('\n############ ########### ##############\n')
