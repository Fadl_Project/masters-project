import numpy as np
import os
import cv2


def main():
    # define here the output path for the generated .npy images
    outPath = "/home/student2/Downloads/Latest_dataset/Part1/part1_npy"
    # define here the path of the images to be converted from .png format to .npy format
    path = "/home/student2/Downloads/Latest_dataset/Part1"
    # iterate through the names of contents of the folder

    for i, image_name in enumerate(os.listdir(path)[:100]):
        # create the full input path and read the file
        input_path = os.path.join(path, image_name)
        image = cv2.imread(input_path)

        # BGR -> RGB
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        print(type(image))
        print(input_path)
        # create full output path, 'example.jpg'
        # becomes 'npy_example.jpg', save the file to disk
        fullpath = os.path.join(outPath, 'npy_' + image_name)
        np.save(fullpath, image)
        #if i >= 100:
        #    break

if __name__ == '__main__':
    main()
