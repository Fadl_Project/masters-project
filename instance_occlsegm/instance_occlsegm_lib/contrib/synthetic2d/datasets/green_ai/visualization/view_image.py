"""
Time: 26.04.2022
author: I. fadl
"""
import matplotlib.pyplot as plt
import numpy as np
import cv2

# npy_lbl_cls.npy visualization
lbl_cls = np.load('000000/lbl_cls.npy')
print('npy_lbl_cls.dtype: ', lbl_cls.dtype)
print('npy_lbl_cls.shape: ', lbl_cls.shape)
plt.imshow(lbl_cls)
plt.title('npy_lbl_cls.npy')
plt.show()

"""
# masks.npy visualization
masks = np.load('000000/masks.npy')
print('masks.dtype: ', masks.dtype)
print('masks.shape: ', masks.shape)
plt.imshow(masks[0])
plt.title('masks.npy')
plt.show()

# img.npy visualization
img = np.load('000000/img.npy')
print('img.dtype: ', img.dtype)
print('img.shape: ', img.shape)
plt.imshow(img)
plt.title('img.npy')
plt.show()
"""

def make_lbl_cls(im):
    """
    Args:
        im: *im.png

    Returns:
        npy_lbl_cls.npy
    """
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    im = cv2.resize(im, (640, 480), interpolation=cv2.INTER_NEAREST)
    im = im[:, :, 2]  # visible masks
    im = im / 4
    lbl_cls = np.array(im, dtype='int32')
    np.save('npy_lbl_cls', lbl_cls)
    lbl_cls = np.load('lbl_cls.npy')
    return lbl_cls

# convert *im.png to npy_lbl_cls.npy
im = cv2.imread('Package_01_synthetic_1_1_im.png')
lbl_cls = make_lbl_cls(im)
print('\nnpy_lbl_cls.dtype: ', lbl_cls.dtype)
print('npy_lbl_cls.shape: ', lbl_cls.shape)
plt.imshow(lbl_cls)
plt.title('npy_lbl_cls.npy')
plt.show()
