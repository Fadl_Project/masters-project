import numpy as np
import os
import matplotlib.pyplot as plt

cwd = os.getcwd()  # Get the current working directory (cwd)
dir_path = sorted(os.listdir(cwd))  # Get all the files in that directory ordered alphabetically
#print("Files in %r: %s" % (cwd, dir_path))
print(dir_path)

img_array_2 = np.load('img_3.npy')
print(type(img_array_2))
print(img_array_2.dtype)
plt.imshow(img_array_2)
plt.show()

