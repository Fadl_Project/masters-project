import numpy as np
import os
import cv2
import sys
import matplotlib.pyplot as plt

def main():
    """
    plt.imshow(img)
    plt.title('img')
    plt.show()
    """
    img_array = np.load('img.npy')
    plt.imshow(img_array)
    plt.title('img')
    plt.show()
    print(img_array.dtype)
    # print(img_array)


    bboxes_array = np.load('bboxes.npy')
    print(bboxes_array.dtype)
    print(bboxes_array)


    labels_array = np.load('labels.npy')
    print(labels_array.dtype)
    print(labels_array)

    # what about showing this .npy file as a list or a txt file?
    print((type(labels_array)))
    print(sys.getsizeof(labels_array))  # size in Bytes


    # print(masks_array.size()) # ERROR: TypeError: 'tuple' object is not callable
    masks_array = np.load('masks.npy')
    print(masks_array.dtype)
    # plt.imshow(masks_array)
    # plt.title('Masks')
    # plt.show()


    # plt.imshow(masks_array) # mask array is tuple of numpy darrays
    # plt.show()
    # TypeError: Invalid shape (7, 480, 640) for image data
    print(masks_array.dtype)
    print(type(masks_array))
    print(np.max(masks_array[5]))


    plt.imshow(masks_array[1,:,:]) # mask array is  of numpy darrays
    plt.title('mask1')
    plt.show()
    print(np.max(masks_array[1,:,:])) # 1 is visible, 2 for occluded part. visualize-
    # better on matplotlib in pycharm to double-check. (confirmed!)
    print(masks_array.dtype)
    print(type(masks_array))


    plt.imshow(masks_array[3,:,:]) # mask array is  of numpy darrays
    plt.title('mask3')
    plt.show()
    print(np.max(masks_array[1,:,:]))

    plt.imshow(masks_array[2,:,:]) # mask array is  of numpy darrays
    plt.title('mask2')
    plt.show()
    print(np.max(masks_array[1,:,:]))

if __name__ == '__main__':
    main()
