from prepare_dataset_functions import *
import os
import cv2
import numpy as np
import matplotlib.pyplot as plt

def main():
    # Output path for the generated .npz archives.
    outPath = "/home/student2/Downloads/Latest_dataset/Part1/npz_greenai"
    #outPath = 'home/student2/PycharmProjects/masterproject/relook/instance_occlsegm/instance_occlsegm_lib/contrib/synthetic2d/datasets/green_ai/generatednpz/'
    # Input images to be converted from .jpg format to .npy format.
    path = "/home/student2/Downloads/Latest_dataset/Part1/rgb_jpg"
    path = "/home/student2/Downloads/Latest_dataset/Part1/original/rgb"

    # Input path for the masks *im.png. we get labels, masks and bboxes from it.
    masks_path = "/home/student2/Downloads/Latest_dataset/Part1/masks_jpg"
    masks_path = "/home/student2/Downloads/Latest_dataset/Part1/original/masks"
    # Order images in 'masks_path' alphabetically.
    masks_dir_path = sorted((os.listdir(masks_path)))

    for mask_index, mask_name in enumerate(masks_dir_path):
        # Index every image in 'dir_path' and get ground truth data from them.
        if mask_name.endswith("im.png"):    # typical size is 20-30kb for .jpg
            # Only work with '*im.jpg' images from GreenAI dataset
            # make full path of the image.
            input_path = os.path.join(masks_path, mask_name)
            print(mask_name)
            """ create image array"""
            # read, convert, resize, select channels. Blue channel: visible, Red channel: occluded.
            mask_image = cv2.imread(input_path)
            mask_image = cv2.cvtColor(mask_image, cv2.COLOR_BGR2RGB) # ESSENTIAL!!
            #mask_image = cv2.resize(mask_image, (640, 480), interpolation=cv2.INTER_NEAREST)
            #mask_image = cv2.imread(mask_name)
            # show the visible instances from the second chanel.
            mask_image_v = mask_image[:, :, 2]  # visible instances, Blue chanel.
            # show the occluded mask from the 0 chanel.
            mask_image_o = mask_image[:, :, 0]  # occluded segment from an instance, Red chanel.

            """ create label array"""
            labels_o = get_occluded_labels(mask_image_o)
            labels_v = get_visible_labels(mask_image_v)
            labels = labels_v/4   # array
            labels = np.array(labels, dtype='int32')

            """ create npy_lbl_cls.npy """
            lbl_cls = make_lbl_cls(mask_image)

            """ create masks array """
            repeated_instances = get_repeated_instances(labels_v, labels_o)
            dic_instances_masks = create_mask_instances_dic(labels_v, mask_image_v, mask_image_o, repeated_instances)
            masks_list = list_of_arrays(dic_instances_masks)
            masks = masks_list  # array
            masks = np.array(masks, dtype='int32')  # instead of unit8

            """ create bboxes array """
            bboxes = get_bboxes(dic_instances_masks)    # array
            bboxes = np.array(bboxes, dtype='int32')

            """ create img.npy """
            image_name = mask_name.replace('_im', '_rgb')
            input_path = os.path.join(path, image_name)  # Good, necessary to get the image itself not only the name of it.
            image = cv2.imread(input_path)  # Good.
            img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) # ESSENTIAL!!
            """ create .npz """
            base_name = os.path.splitext(os.path.basename(mask_name))[0]
            npz_dir_name = base_name.replace('_im', '')
            npz_dir_name = os.path.join(outPath, npz_dir_name)
            np.savez(npz_dir_name, img=img, bboxes=bboxes, masks=masks, labels=labels,
                     lbl_cls=lbl_cls)  # np.savez(npz_archive_name, npy_file_name_1 = array_1, ...)
            print('saved ', npz_dir_name, ' successfully!\n')
        # processing 100 image needs 5.0 GB of free memory. Outdated
        # Currently 1 .npz is 4.6 MB , 1000 .npz takes around 4.6 GB. Original dataset: 500KB for 1 .npz.
        if mask_index >= 10:
            break


if __name__ == '__main__':
    main()
    print("\nSuccessfully saved .npz archive for the images.")
