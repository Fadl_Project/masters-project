"""
Author: Islam Fadl
Date: 31.01.2022
"""
import cv2
import os
from matplotlib import pyplot as plt
from prepare_dataset_functions import *

def main():
    PATH = "/home/student2/Downloads/Latest_dataset/Part1/original/masks"
    dir_files = sorted(os.listdir(PATH))  # Get all the files in that directory ordered alphabetically
    print(dir_files)
    normalizing_int = 4
    class_num = []

    for image_index, image_name in enumerate(dir_files):
        if image_name.endswith("im.png"):
            input_path = os.path.join(PATH, image_name)
            img = cv2.imread(input_path)
            labels_v = get_visible_labels(img)  # get the labels in a list
            n_labels = [int(element / normalizing_int) for element in labels_v]
            # add the class numbers to a list
            for i in n_labels:
                first_int = int(str(i)[0])
                class_num.append(first_int)
                class_num = list(set(class_num))  # removes duplicates
            # print(class_num) # list
    print("classes in the images: ", class_num)
    # TODO: 21: 2 is the class, 1 is the instance of that class.
    with open("class_names.txt", "w") as output:
        for cl_num in class_num:
            output.write(str(cl_num) + "\n")
    print('Done! .txt file created in the current dir.')

if __name__ == '__main__':
    main()
