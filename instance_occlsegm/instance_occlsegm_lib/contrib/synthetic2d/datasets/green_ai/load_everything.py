import numpy as np
#import os
import cv2
import matplotlib.pyplot as plt

def main():
    """
    TODO: convert '*im.png' images to masks and bboxes and labels by the load mask file
    TODO: create masks, get label and create bbox for every image ending with '*im.png'
    TODO: create .npy for every bbox, label and mask
    TODO: create a .npz dir for every np_image, bbox, label and mask.
    """
    image_path = "/relook/instance_occlsegm/instance_occlsegm_lib/contrib/synthetic2d/datasets/green_ai/notebooks/Package_01_synthetic_1_1_im.png"
    img = cv2.imread(image_path)
    plt.imshow(img)
    plt.title('img')
    plt.show()
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = img / 4
    img = img.astype(np.uint8)
    plt.imshow(img)
    plt.title('img')
    plt.show()
    img = img[:,:,0]
    plt.imshow(img)
    plt.title('img')
    plt.show()


    histogram = cv2.calcHist([img], [0], None, [256], [0, 256])
    print(type(histogram))
    plt.plot(histogram, color='r')
    plt.show()

    """
    Masks with labels begining with 3 such as 31 or 32, are for occluded masks.
    On the contrary, labels starting with 5, such as 51 or 52 are for visible maks.
    """
    labels = []
    [labels.append(i) for i, l in enumerate(histogram) if i != 0 and l != 0]
    print(type(labels))
    print(labels)

    instances_masks = {}
    for instance_id in labels:
        mask = img == instance_id
        mask = mask.astype(np.uint8)
        instances_masks[instance_id] = cv2.bitwise_and(img, img, mask=mask * 255)
        # instances_masks[instance_id] = mask

    print(instances_masks.keys())

    # Iterate over the keys
    for key in instances_masks:
        # Get masks
        plt.imshow(instances_masks[key])
        plt.title('instance')
        plt.show()
        # Get bboxes
        x, y, z, w = cv2.boundingRect(instances_masks[key])
        print(x, y, z, w)

    _, mask = cv2.threshold(img, 51, 255, cv2.THRESH_BINARY)
    # mask_inv = cv2.bitwise_not(img)
    img_1 = img
    dst = cv2.bitwise_and(img, img_1, mask=mask * 255)

    plt.imshow(mask)
    plt.title('mask')
    plt.show()

    plt.imshow(dst)
    plt.title('dst')
    plt.show()


if __name__ == '__main__':
    main()

"""
# iterate through the names of contents of the folder
for i, image_name in enumerate(dir_path):
    # create the full input path and read the image
    input_path = os.path.join(path, image_name)
    print(input_path)

    #reading images
    mask = cv2.imread(input_path)
    mask = mask / 4
    #cv2.imshow('Original image', mask)

    print(type(mask))
    h, w, c = mask.shape
    print('width:  ', w)
    print('height: ', h)
    print('channel:', c)
    print('Original Dimensions : ', mask.shape)

    # resize
    scale_percent = 40  # percent of original size
    height = int(mask.shape[0] * scale_percent / 100)
    width = int(mask.shape[1] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized_mask = cv2.resize(mask, dim, interpolation=cv2.INTER_AREA)
    print('Resized Dimensions : ', resized_mask.shape)
    cv2.imshow("Resized image", resized_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    mask = get_instances(mask)
    print(type(mask))

    # create full output path, so 'example.jpg' becomes 'npy_example.jpg', save the file to disk.
    fullpath = os.path.join(outPath, 'npy_' + image_name)
    np.save(fullpath, mask)
    if i > 3:
        break



def resizeAndPad(img, size, padColor=0):

    h, w = img.shape[:2]
    sh, sw = size

    # interpolation method
    if h > sh or w > sw: # shrinking image
        interp = cv2.INTER_AREA
    else: # stretching image
        interp = cv2.INTER_CUBIC

    # aspect ratio of image
    aspect = w/h  # if on Python 2, you might need to cast as a float: float(w)/h

    # compute scaling and pad sizing
    if aspect > 1: # horizontal image
        new_w = sw
        new_h = np.round(new_w/aspect).astype(int)
        pad_vert = (sh-new_h)/2
        pad_top, pad_bot = np.floor(pad_vert).astype(int), np.ceil(pad_vert).astype(int)
        pad_left, pad_right = 0, 0
    elif aspect < 1: # vertical image
        new_h = sh
        new_w = np.round(new_h*aspect).astype(int)
        pad_horz = (sw-new_w)/2
        pad_left, pad_right = np.floor(pad_horz).astype(int), np.ceil(pad_horz).astype(int)
        pad_top, pad_bot = 0, 0
    else: # square image
        new_h, new_w = sh, sw
        pad_left, pad_right, pad_top, pad_bot = 0, 0, 0, 0

    # set pad color
    if len(img.shape) == 3 and not isinstance(padColor, (list, tuple, np.ndarray)): # color image but only one color provided
        padColor = [padColor]*3

    # scale and pad
    scaled_img = cv2.resize(img, (new_w, new_h), interpolation=interp)
    scaled_img = cv2.copyMakeBorder(scaled_img, pad_top, pad_bot, pad_left, pad_right, borderType=cv2.BORDER_CONSTANT, value=padColor)

    return scaled_img



# displaying image
mask = resizeAndPad(mask, (720,720), padColor=0)
cv2.imshow('image', mask)
#cv2.waitKey(0)

# define here the output path for the generated masks
outPath = "/home/student2/Downloads/Latest_dataset/Part1/masks_npy"
# define here the path of the images to be converted from .png format to .npy format
path = "/home/student2/Downloads/Latest_dataset/Part1/masks"
dir_path = sorted(os.listdir(path))
"""


