import numpy as np
import os
import cv2

def make_lbl_cls(im):
    """
    Args:
        mask_image_png: *im.png image

    Returns: *.npy file, where values in arrays represent the class of the instance.

    How it works: pixels representing an instance in *im.png are converted to values of the instance class in a numpy array

    Values of pixels:
    instance intensity in npy_lbl_cls.npy  = (instance class (number) - 1). e.g,
    background -1
    Intensity  0 = class number  1 : __background__
    Intensity 27 = class number 28 : plastic_wine_glass
    Intensity 24 = class number 25 : mesh_cup
    """
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    #im = cv2.resize(im, (640, 480), interpolation=cv2.INTER_NEAREST)
    im = im[:, :, 2]  # visible masks
    im = im / 4
    lbl_cls = np.array(im, dtype='int32')
    np.save('npy_lbl_cls', lbl_cls)
    lbl_cls = np.load('lbl_cls.npy')
    return lbl_cls

def get_visible_labels(img_v):
    hist_v = cv2.calcHist([img_v], [0], None, [256], [0, 256])
    labels_v = []
    [labels_v.append(i) for i, l in enumerate(hist_v) if i != 0 and l != 0]
    labels_v = np.array(labels_v, dtype='int32')  # instead of int64
    return labels_v

def get_class_names(labels_v):
    class_names = []
    for i in labels_v:
        i = int(i/4)
        first_int = int(str(i)[0])
        class_names.append(first_int)
        class_names = list(set(class_names))
        #class_names.append(int(str(i[0]))
    return class_names

def get_occluded_labels(img_o):
    hist_o = cv2.calcHist([img_o], [0], None, [256], [0, 256])
    labels_o = []
    [labels_o.append(i) for i, l in enumerate(hist_o) if i != 0 and l != 0]
    return labels_o


def get_repeated_instances(labels_v, labels_o):
    repeated_instances = []
    for i in labels_v:
        if i in labels_o:
            repeated_instances.append(i)
    return repeated_instances


def create_mask_instances_dic(labels_v, img_v, img_o, repeated_instances):
    # make a dictionary containing pairs of instance label (key) and instance mask (value).
    dic_instances_masks = {}
    for instance_id in labels_v:
        if instance_id in repeated_instances:
            # combine visible and occluded parts of an instance into one mask.
            mask_o = img_o == instance_id
            mask_o = mask_o.astype(np.uint8)
            mask_v = img_v == instance_id
            mask_v = mask_v.astype(np.uint8)
            mask = mask_v + mask_o * 2
            # Applies the mask to the original image, expect binary values: 0,1 only! ((((CHECK!!!))))
            dic_instances_masks[instance_id] = cv2.bitwise_and(img_v, img_v, mask=mask * 255)

        else:
            # create mask for each instance id in the labels list.
            mask = img_v == instance_id  # assign 1 to pixels which have instance_id intensity value, 0 otherwise.
            mask = mask.astype(np.uint8)
            # Applies the mask to the original image, expect binary values: 0,1 only! ((((CHECK!!!))))
            dic_instances_masks[instance_id] = cv2.bitwise_and(img_v, img_v, mask=mask * 255)
    return dic_instances_masks


def list_of_arrays(dic_instances_masks):
    # create list of arrays (list of instances masks) for every image in the dataset
    masks_list = []
    for key in dic_instances_masks:
        mask_instance = dic_instances_masks[key]
        # masks_list = np.append(masks_list, mask_instance)
        masks_list.append(mask_instance)
    return masks_list


def get_bboxes(dic_instances_masks):
    # Iterate over the keys to get bboxes:
    bboxes = []
    for key in dic_instances_masks:
        x, y, z, w = cv2.boundingRect(dic_instances_masks[key])
        bbox_list = [x, y, z, w]
        bboxes.append(bbox_list)
    bboxes = np.asarray(bboxes)
    bboxes = np.array(bboxes, dtype='int32')  # instead of unit8
    return bboxes