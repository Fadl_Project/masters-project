import numpy as np
import os
import cv2
import matplotlib.pyplot as plt


def get_labels(img):
    hist = cv2.calcHist([img], [0], None, [256], [0, 256])
    labels= []
    [labels.append(i) for i, l in enumerate(hist) if i != 0 and l != 0]
    print('labels are: ', labels)
    return

def get_bounding_boxes(instances_masks, key):
    x, y, z, w = cv2.boundingRect(instances_masks[key])
    # return a text file containing a list for every instance for a given image
    return x, y, z, w

def get_mask(img, labels):
    instances_masks = {}
    for instance_id in labels:
        mask = img == instance_id  # assign 1 to pixels which have instance_id intensity value, 0 otherwise.
        # print(mask.dtype) : bool
        mask = mask.astype(np.uint8)
        # Applies the mask to the original image, expect binary values: 0,255 only!
        instances_masks[instance_id] = cv2.bitwise_and(img, img, mask=mask * 255)
    print(instances_masks.keys())
    # Iterate over the keys to get masks:
    for key in instances_masks:
        plt.imshow(instances_masks[key])
    return

def main():


    # define here the output path for the generated .npy mask
    outPath = "/home/student2/Downloads/Latest_dataset/Part1/masks_npy/labels"
    # define here the path of the images to be converted from .png format to .npy format
    path = "/home/student2/Downloads/Latest_dataset/Part1/masks"
    dir_path = sorted(os.listdir(path))

    # iterate through the names of contents of the folder
    for i, mask in enumerate(dir_path):
        if mask.endswith("im.png"):
            # create the full input path and read the file
            input_path = os.path.join(path, mask)
            mask = cv2.imread(input_path)

        # create full output path, so 'example.jpg' becomes 'npy_example.jpg', save the file to disk.
        fullpath = os.path.join(outPath, 'npy_' + mask)
        np.save(fullpath, mask)


        img = cv2.imread(image_path)
        plt.imshow(img)
        plt.title('img')
        plt.show()
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = img / 4
        img = img.astype(np.uint8)
        plt.imshow(img)
        plt.title('img')
        plt.show()
        img = img[:,:,0]
        plt.imshow(img)
        plt.title('img')
        plt.show()


        histogram = cv2.calcHist([img], [0], None, [256], [0, 256])
        print(type(histogram))
        plt.plot(histogram, color='r')
        plt.show()

        """
        Masks with labels begining with 3 such as 31 or 32, are for occluded masks.
        On the contrary, labels starting with 5, such as 51 or 52 are for visible maks.
        """
        labels = []
        [labels.append(i) for i, l in enumerate(histogram) if i != 0 and l != 0]
        print(type(labels))
        print(labels)

        instances_masks = {}
        for instance_id in labels:
            mask = img == instance_id
            mask = mask.astype(np.uint8)
            instances_masks[instance_id] = cv2.bitwise_and(img, img, mask=mask * 255)
            # instances_masks[instance_id] = mask

        print(instances_masks.keys())

        # Iterate over the keys
        for key in instances_masks:
            # Get masks
            plt.imshow(instances_masks[key])
            plt.title('instance')
            plt.show()
            # Get bboxes
            x, y, z, w = cv2.boundingRect(instances_masks[key])
            print(x, y, z, w)

        _, mask = cv2.threshold(img, 51, 255, cv2.THRESH_BINARY)
        # mask_inv = cv2.bitwise_not(img)
        img_1 = img
        dst = cv2.bitwise_and(img, img_1, mask=mask * 255)

        plt.imshow(mask)
        plt.title('mask')
        plt.show()

        plt.imshow(dst)
        plt.title('dst')
        plt.show()


if __name__ == '__main__':
    main()
