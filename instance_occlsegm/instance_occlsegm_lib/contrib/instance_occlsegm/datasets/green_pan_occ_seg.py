from glob import glob
import os
import os.path as osp

import chainer
from chainer.dataset import DatasetMixin
import numpy as np

import cv2 as cv

#import instance_occlsegm_lib

#from chainer import dataset
#from instance_occlsegm_lib.contrib.synthetic2d.datasets.arc2017_occlusion \
#import _load_npz




class NewDataset(DatasetMixin): # you will inherit something here, learn about it from chainer dataset documentation
#class chainer.datasets.ImageDataset(paths, root='.', dtype=None)

#path1 /home/student2/Downloads/GreenAI_dataset/Real_Dataset
#path2 /home/student2/Downloads/GreenAI_dataset/synthetic_small_dataset
    def __init__(self, data_dir='/home/student2/Downloads/GreenAI_dataset/Real_Dataset', split='train'):
        self.data_dir = data_dir
        #/home/student2/Downloads/GreenAI_dataset/synthetic_small_dataset

        img_paths = np.array(sorted(glob(self.data_dir + '/*rgb.png')))
        label_paths = np.array(sorted(glob(self.data_dir + '/*im.png')))
        assert len(img_paths) == len(label_paths), "Please check your code ya bo2la"
        n_images = len(img_paths)
        # for example: 80% training, 20% validation
        all_indices = np.arange(len(img_paths))
        train_indices = np.random.choice(all_indices, int(0.8*n_images), replace=False)
        val_indices = [i for i in all_indices if not i in train_indices]
        if split == 'train':
            self.img_paths = img_paths[train_indices]
            self.label_paths = label_paths[train_indices]
        elif split == 'val':
            self.img_paths = img_paths[val_indices]
            self.label_paths = label_paths[val_indices]

        print("Number of {0} images = {1}".format(split, len(self.img_paths)))


    def __len__(self):
        return len(self.img_paths)

    def get_example(self, index):
        img_path = self.img_paths[index]
        label_path = self.label_paths[index]

        img = cv.imread(img_path)
        label =cv.imread(label_path, 0)

        return img, label