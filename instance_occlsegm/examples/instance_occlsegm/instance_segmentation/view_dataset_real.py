#!/usr/bin/env python
import os
import sys
# Root directory of the instance_occlsegm_lib package we need to install
ROOT_DIR = os.path.abspath("../../../")
# Import instance_occlsegm_lib
sys.path.append(ROOT_DIR)  # To find local version of the library
import instance_occlsegm_lib


def main():
    dataset = instance_occlsegm_lib.datasets.apc.\
        ARC2017InstanceSegmentationDataset(split='train', aug='standard')

    instance_occlsegm_lib.datasets.view_instance_seg_dataset(dataset)


if __name__ == '__main__':
    main()
