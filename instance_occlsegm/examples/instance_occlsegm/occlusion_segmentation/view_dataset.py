#!/usr/bin/env python

import os
import sys
# Root directory of the instance_occlsegm_lib package we need to install
ROOT_DIR = os.path.abspath("../../../")

# Import instance_occlsegm_lib
sys.path.append(ROOT_DIR)  # To find local version of the library


from instance_occlsegm_lib.contrib import instance_occlsegm


if __name__ == '__main__':
    data = instance_occlsegm.datasets.OcclusionSegmentationDataset('train')
    data.split = 'train'
    instance_occlsegm.datasets.view_occlusion_segmentation_dataset(data)
